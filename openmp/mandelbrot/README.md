# OpenMP - Mandelbrot

Pour cette partie, nous allons réutiliser le code de la partie sur l'espace de Mandelbrot, mais le paralléliser sur les coeurs d'un processeur.

## Question 3 - Parallélisation et Résultats

```c
#include <omp.h>

omp_set_num_threads(4);

/* Traitement de la grille point par point */
#pragma omp parallel 
{ 
    #pragma omp for private(j, y, x, pima) schedule(auto)
    for (i = 0; i < h; i++) {	
        pima = &ima[i*w];
        x = xmin;
        y = ymin + i*yinc;
        for (j = 0; j < w; j++) {
        // printf("%d\n", xy2color( x, y, prof));
        // printf("(x,y)=(%g;%g)\t (i,j)=(%d,%d)\n", x, y, i, j);
        *pima++ = xy2color( x, y, prof); 
        x += xinc;
        }
        y += yinc; 
    }
}
```

```
./mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100
```

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 0,291 | 1 | 1
2 | 0,173 | 1,682 | 0,841
4 | 0,097 | 3 | 0,750
8 | 0,096 | 3,031 | 0,379
16 | 0,097 | 3 | 0,188
32 | 0,092 | 3,163 | 0,099
64 | 0,092 | 3,163 | 0,049
128 | 0,091 | 3,198 | 0,025
256 | 0,092 | 3,163 | 0,012

![alt text](openmp/mandelbrot/images/TempsExecution.png "Temps d'éxecution")
![alt text](openmp/mandelbrot/images/Acceleration.png "Accélération")
![alt text](openmp/mandelbrot/images/TempsExecution.png "Efficacité")