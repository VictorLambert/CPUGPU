# OpenMP

Pour cette partie, nous allons réutiliser le code de la partie sur l'espace de Mandelbrot ainsi que celui sur la convolution, mais les paralléliser sur les coeurs d'un processeur.

## Question 1 - Nombre de coeur disponibles

Détermination du nombre de coeur disponible sur la machine pour ensuite utiliser la librairie de parallélisation OpenMP.
```
cat /proc/cpuinfo
```
```
processor	: 0
cpu cores	: 4
```

## Question 3 - Mandelbrot Parallélisation et Résultats

```c
#include <omp.h>

omp_set_num_threads(4);

/* Traitement de la grille point par point */
#pragma omp parallel 
{ 
    #pragma omp for private(j, y, x, pima) schedule(auto)
    for (i = 0; i < h; i++) {	
        pima = &ima[i*w];
        x = xmin;
        y = ymin + i*yinc;
        for (j = 0; j < w; j++) {
        // printf("%d\n", xy2color( x, y, prof));
        // printf("(x,y)=(%g;%g)\t (i,j)=(%d,%d)\n", x, y, i, j);
        *pima++ = xy2color( x, y, prof); 
        x += xinc;
        }
        y += yinc; 
    }
}
```
```
./mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100
```

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 0,291 | 1 | 1
2 | 0,173 | 1,682 | 0,841
4 | 0,097 | 3 | 0,750
8 | 0,096 | 3,031 | 0,379
16 | 0,097 | 3 | 0,188
32 | 0,092 | 3,163 | 0,099
64 | 0,092 | 3,163 | 0,049
128 | 0,091 | 3,198 | 0,025
256 | 0,092 | 3,163 | 0,012

![alt text](openmp/mandelbrot/images/TempsExecution.png "Temps d'éxecution")
![alt text](openmp/mandelbrot/images/Acceleration.png "Accélération")
![alt text](openmp/mandelbrot/images/TempsExecution.png "Efficacité")

## Question 4 - Convolution Parallélisation et Résultats

```c
#include <omp.h>

#pragma omp parallel 
{ 
    #pragma omp for private(j) schedule(auto)    
    /* on laisse tomber les bords */
    for(i=1 ; i<nbl-1 ; i++) {
      for(j=1 ; j<nbc-1 ; j++){
        tmp[i*nbc+j] = filtre(
            choix,
            tab[(i+1)*nbc+j-1],tab[(i+1)*nbc+j],tab[(i+1)*nbc+j+1],
            tab[(i  )*nbc+j-1],tab[(i)*nbc+j],tab[(i)*nbc+j+1],
            tab[(i-1)*nbc+j-1],tab[(i-1)*nbc+j],tab[(i-1)*nbc+j+1]);
      } /* for j */
    } /* for i */
}

```
```
./convol Albert-Einstein.ras 4 10
```

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 2,98 | 1 | 1
2 | 1,502 | 1,984 | 0,992
4 | 0,889 | 3,352 | 0,838
8 | 0,897 | 3,322 | 0,415
16 | 0,852 | 3,498 | 0,219
32 | 0,842 | 3,539 | 0,111
64 | 0,835 | 3,569 | 0,056
128 | 0,837 | 3,560 | 0,028
256 | 0,843 | 3,535 | 0,014

![alt text](openmp/convol/images/TempsExecution.png "Temps d'éxecution")
![alt text](openmp/convol/images/Acceleration.png "Accélération")
![alt text](openmp/convol/images/TempsExecution.png "Efficacité")