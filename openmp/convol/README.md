# OpenMP - Convolution

Pour cette partie, nous allons réutiliser le code de la partie sur le calcul de convolution, mais le paralléliser sur les coeurs d'un processeur.

## Question 4 - Parallélisation et Résultats

```c
#include <omp.h>

#pragma omp parallel 
{ 
    #pragma omp for private(j) schedule(auto)    
    /* on laisse tomber les bords */
    for(i=1 ; i<nbl-1 ; i++) {
      for(j=1 ; j<nbc-1 ; j++){
        tmp[i*nbc+j] = filtre(
            choix,
            tab[(i+1)*nbc+j-1],tab[(i+1)*nbc+j],tab[(i+1)*nbc+j+1],
            tab[(i  )*nbc+j-1],tab[(i)*nbc+j],tab[(i)*nbc+j+1],
            tab[(i-1)*nbc+j-1],tab[(i-1)*nbc+j],tab[(i-1)*nbc+j+1]);
      } /* for j */
    } /* for i */
}

```

```
./convol Albert-Einstein.ras 4 10
```

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 2,98 | 1 | 1
2 | 1,502 | 1,984 | 0,992
4 | 0,889 | 3,352 | 0,838
8 | 0,897 | 3,322 | 0,415
16 | 0,852 | 3,498 | 0,219
32 | 0,842 | 3,539 | 0,111
64 | 0,835 | 3,569 | 0,056
128 | 0,837 | 3,560 | 0,028
256 | 0,843 | 3,535 | 0,014

![alt text](openmp/convol/images/TempsExecution.png "Temps d'éxecution")
![alt text](openmp/convol/images/Acceleration.png "Accélération")
![alt text](openmp/convol/images/TempsExecution.png "Efficacité")