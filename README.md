# Programmation parallèle

## Auteur

* **Victor Lambert**

# I. Ensemble de Mandelbrot

## Question 1 - Choix techniques pour le calcul séquentiel

```c
/* Image resultat */
unsigned char	*ima, *pima;

/* Allocation memoire du tableau resultat */  
pima = ima = (unsigned char *)malloc( w*h*sizeof(unsigned char));
```

Définition d'un tableau à deux dimensions comprenant l'image en sortie. On parcourt les pixels de l'image et en fonction de leur position, on ajoute à l'image résultante la valeur correspondante. Pour ce faire, on calcul en la valeur de Mandelbrot associée à chaque coordonnée à l'aide d'une boucle selon la valeur affectée à la profondeur.

## Question 2 - Résultats sans parallélisation

### Aide

```
Usage:
      mandel dimx dimy xmin ymin xmax ymax prof

      dimx,dimy : dimensions de l'image a generer
      xmin,ymin,xmax,ymax : domaine a calculer dans le plan complexe
      prof : nombre maximale d'iteration

Quelques exemples d'execution
      mandel 800 800 0.35 0.355 0.353 0.358 200
      mandel 800 800 -0.736 -0.184 -0.735 -0.183 500
      mandel 800 800 -0.736 -0.184 -0.735 -0.183 300
      mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100
      mandel 800 800 -1.5 -0.1 -1.3 0.1 10000
```

### Exemple 1

- dimx : 800
- dimy : 800
- xmin : 0.35
- ymin : 0.355
- xmax : 0.353
- ymax : 0.358
- prof : 200

```
Domaine: {[0.35,0.355]x[0.353,0.358]}
Increment : 3.75469e-06 3.75469e-06
Prof: 200
Dim image: 800x800
Temps total de calcul : 0.368491 sec
0.368491
```

### Exemple 2

- dimx : 800
- dimy : 800
- xmin : -0.736
- ymin : -0.184
- xmax : -0.735
- ymax : -0.183
- prof : 500

```
Domaine: {[-0.736,-0.184]x[-0.735,-0.183]}
Increment : 1.25156e-06 1.25156e-06
Prof: 500
Dim image: 800x800
Temps total de calcul : 1.21015 sec
1.21015
```

### Exemple 3

- dimx : 800
- dimy : 800
- xmin : -0.736
- ymin : -0.184
- xmax : -0.735
- ymax : -0.183
- prof : 300

```
Domaine: {[-0.736,-0.184]x[-0.735,-0.183]}
Increment : 1.25156e-06 1.25156e-06
Prof: 300
Dim image: 800x800
Temps total de calcul : 1.02516 sec
1.02516
```

### Exemple 4

- dimx : 800
- dimy : 800
- xmin : -1.48478
- ymin : 0.00006
- xmax : -1.48440
- ymax : 0.00044
- prof : 100

```
Domaine: {[-1.48478,6e-05]x[-1.4844,0.00044]}
Increment : 4.75594e-07 4.75594e-07
Prof: 100
Dim image: 800x800
Temps total de calcul : 0.286256 sec
0.286256
```

### Exemple 5

- dimx : 800
- dimy : 800
- xmin : -1.5
- ymin : -0.1
- xmax : -1.3
- ymax : 0.1
- prof : 10000

```
Domaine: {[-1.5,-0.1]x[-1.3,0.1]}
Increment : 0.000250313 0.000250313
Prof: 10000
Dim image: 800x800
Temps total de calcul : 8.79786 sec
8.79786
```

### Comparaison

Les exemples ci-dessus sont ceux donnés par la documentation d'information du programme. Ce paragraphe commente les résultats obtenus.

D'après les résultats observés, plus le paramètre de profondeur est élevé et plus le temps total de calcul est long. Ce paramètre joue donc sur le temps d'exécution. En revanche, l'incrémentation ne semble pas jouer sur le temps d'exécution, ce qui semble normal car ce paramètre ne joue pas sur la taille qu'aurons les boucles dans le programme.

## Question 3 - Calculs parallèles

### Problématique

Le calcul des valeurs de l'ensemble de Mandelbrot en elle-mêmes est difficile à paralléliser car comme on le voit ci-dessous, chaque tour de boucle (mis à part le premier) va déprendre du résultat obtenu au tour de boucle précédent. De ce fait, chaque exécution en parallèle de cette fonction devra attendre la fin de l'exécution de la partie d'avant.

```c
unsigned char xy2color(double a, double b, int prof) {
    double x, y, temp, x2, y2;
    int i;

    x = y = 0.;
    for( i=0; i<prof; i++) {
        /* garder la valeur précédente de x qui va etre ecrase */
        temp = x;
        /* nouvelles valeurs de x et y */
        x2 = x*x;
        y2 = y*y;
        x = x2 - y2 + a;
        y = 2*temp*y + b;
        if( x2 + y2 >= 4.0) break;
    }
    return (i==prof)?255:(int)((i%255));
}
```

### Parallélisation

En revanche, la boucle de parcours de la grille pour le calcul de la valeur associée est totalement parallélisable car chaque itération est indépendante!

```c
/* Dimension de l'image */
int w,h;
/* Variables intermediaires */
int  i, j;
double x, y;
/* Traitement de la grille point par point */
y = ymin;
for (i = 0; i < h; i++) {
    x = xmin;
    for (j = 0; j < w; j++) {
        // printf("%d\n", xy2color( x, y, prof));
        // printf("(x,y)=(%g;%g)\t (i,j)=(%d,%d)\n", x, y, i, j);
        *pima++ = xy2color( x, y, prof);
        x += xinc;
    }
    y += yinc;
}
```

### Première approche

- Modèle : maître-esclave
- Distribution : 1D de données 2D par bloc
- Mode : Statique

Le maître ne va pas envoyer de données. Chaque processeur connaît :

- Hauteur de l'image
- Largeur de l'image
- Nombre de processeurs
- Taille de son bloc

Chacun va calculer les valeurs résultantes de son bloc dans l'ensemble de Mandelbrot et le stocker à une adresse. Une fois le calcul fini, il va transmettre cette adresse au maître. Celui-ci pourra alors concaténer les résultats une fois tous les blocs calculés.

- mpirun -np 4 ./mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100
- Temps total de calcul : 0.11397 sec
- Résultat : Temps divisé par deux.

### Deuxièmes approche

- Modèle : maître-esclave
- Distribution : 1D de données 2D par ligne
- Mode : Dynamique

Chaque processeur connaît :

- Hauteur de l'image
- Largeur de l'image
- Nombre de processeurs
- Taille de son bloc

Le maître va stocker une pile d'exécution, chaque objet de la pile représentant l'adresse mémoire d'une ligne de l'image à calculer. Chaque processeur va réaliser le calcul pour une ligne. L'adresse de cette ligne lui sera transmise par le maître et il lui retournera le résultat une fois finie. Tant que la pile d'exécution n'est pas vide, à chaque fois qu'un processeur aura fini une ligne, le maître lui enverra l'adresse d'une nouvelle ligne à calculer. Chaque ligne sera concaténée aux autres dès qu'elle sera reçue.

## Question 4 - Analyse des performances

Pour analyser les performances, nous utilisons l'exemple './mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100' avec différents nombre d'instances. Ces instances seront répartis sur les processeurs d'une seule machine.

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
2 | 0,306 | 1 | 0,5
4 | 0,171 | 1,789 | 0,447
8 | 0,254 | 1,204 | 0,150
16 | 0,433 | 0,706 | 0,044
32 | 0,615 | 0,497 | 0,015
64 | 1,04 | 0,294 | 0,004
128 | 2,124 | 0,144 | 0,001

![alt text](mandelbrot/images/TempsExecution.png "Temps d'éxecution")
![alt text](mandelbrot/images/Acceleration.png "Accélération")
![alt text](mandelbrot/images/Efficacite.png "Efficacité")

# II. Convolution

## Question 1 - Utilisation d'un tampon mémoire

```c
unsigned char *tmp;
/* Allocation memoire du tampon intermediaire : */
tmp = (unsigned char*) malloc(sizeof(unsigned char) *nbc*nbl);
```

Un tampon intermédiaire est nécessaire pour réaliser la convolution, car il est nécessaire de toujours avoir le pixel d'origine pour réaliser le calcul de convolution sur ses pixels voisins. Ainsi, si un pixel d'origine est modifié, sa valeur de départ ne pourra plus être utilisée pour le calcul, et ainsi la convolution résultant sera fausse. On utilise donc un tampon intermédiaire pour stocker les nouveaux pixels, à partir des valeurs de l'image de base.

## Question 2 - Séquences parallélisables

Sachant que l'on utilise un tampon intermédiaire pour le calcul de convolution, il est alors possible de parallélisé les boucles qui appliquent le filtre à l'image.

```c
int w, h; /* nombre de lignes et de colonnes de l'image */
/* Variables liees au traitement de l'image */
int filtre;	/* numero du filtre */
int nbiter;	/* nombre d'iterations */
/* La convolution a proprement parler */
for(i=0 ; i < nbiter ; i++){
    convolution(filtre, r.data, h, w);
}
```

## Question 3 - Complexité et équilibrage

L'algorithme réalise un parcours de l'image I par double boucle. Sachant que la noyau k de convolution est de 3x3, la complexité théorique du calcul d'un pixel de I*k est donc constante et égale à O(1).

## Question 4 - Découpage

Le temps de calcul de la convolution en un pixel étant constant, le découpage naturel du calcul entre les processeurs serait un découpage par bloc statique.

## Question 5 - Problèmes au bord

Entre chaque convolution, les valeurs au bord de l'image restent les mêmes qu'à l'origine. Ainsi, plus le nombre d'opérations de convolution est élevée, plus la convolution sera faussé car cette erreur s'étendra aux pixels voisins aux bords petit à petit, faussant les valeurs de ces derniers et s'étendant aux autres pixels voisins de ceux-ci.

## Question 6 - Résultats

Paramètres:
- Image "Albert-Einstein.ras" 1000*1200 px
- Filtre extracteur de contour
- 20 itérations

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 6.01 | 1 | 1
2 | 3.14 | 1,914 | 0,957
4 | 1.65 | 3,642 | 0,911
7 | 1.06 | 5,669 | 0,809
14 | 0.7 | 8,586 | 0,613
25 | 0.6 | 10,017 | 0,401
28 | 0.58 | 10,362 | 0,37
50 | 1.3 | 4,623 | 0,092
100 | 3.43 | 1,752 | 0,017

![alt text](convol/images/TempsExecution.png "Temps d'éxecution")
![alt text](convol/images/Acceleration.png "Accélération")
![alt text](convol/images/Efficacite.png "Efficacité")

# III. OpenMP

Pour cette partie, nous allons réutiliser le code de la partie sur l'espace de Mandelbrot ainsi que celui sur la convolution, mais les paralléliser sur les cœurs d'un processeur.

## Question 1 - Nombre de cœurs disponibles

Détermination du nombre de cœurs disponible sur la machine pour ensuite utiliser la librairie de parallélisation OpenMP.
```
cat /proc/cpuinfo
```
```
processor	: 0
cpu cores	: 4
```

## Question 3 - Mandelbrot Parallélisation et Résultats

```
./mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100
```

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 0,291 | 1 | 1
2 | 0,173 | 1,682 | 0,841
4 | 0,097 | 3 | 0,750
8 | 0,096 | 3,031 | 0,379
16 | 0,097 | 3 | 0,188
32 | 0,092 | 3,163 | 0,099
64 | 0,092 | 3,163 | 0,049
128 | 0,091 | 3,198 | 0,025
256 | 0,092 | 3,163 | 0,012

![alt text](openmp/mandelbrot/images/TempsExecution.png "Temps d'éxecution")
![alt text](openmp/mandelbrot/images/Acceleration.png "Accélération")
![alt text](openmp/mandelbrot/images/Efficacite.png "Efficacité")

## Question 4 - Convolution Parallélisation et Résultats

```c
#include <omp.h>

#pragma omp parallel
{
    #pragma omp for private(j) schedule(auto)    
    /* on laisse tomber les bords */
    for(i=1 ; i<nbl-1 ; i++) {
      for(j=1 ; j<nbc-1 ; j++){
        tmp[i*nbc+j] = filtre(
            choix,
            tab[(i+1)*nbc+j-1],tab[(i+1)*nbc+j],tab[(i+1)*nbc+j+1],
            tab[(i  )*nbc+j-1],tab[(i)*nbc+j],tab[(i)*nbc+j+1],
            tab[(i-1)*nbc+j-1],tab[(i-1)*nbc+j],tab[(i-1)*nbc+j+1]);
      } /* for j */
    } /* for i */
}

```

```
./convol Albert-Einstein.ras 4 10
```

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 2,98 | 1 | 1
2 | 1,502 | 1,984 | 0,992
4 | 0,889 | 3,352 | 0,838
8 | 0,897 | 3,322 | 0,415
16 | 0,852 | 3,498 | 0,219
32 | 0,842 | 3,539 | 0,111
64 | 0,835 | 3,569 | 0,056
128 | 0,837 | 3,560 | 0,028
256 | 0,843 | 3,535 | 0,014

![alt text](openmp/convol/images/TempsExecution.png "Temps d'éxecution")
![alt text](openmp/convol/images/Acceleration.png "Accélération")
![alt text](openmp/convol/images/Efficacite.png "Efficacité")
