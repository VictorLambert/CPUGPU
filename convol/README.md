# Convolution

## Question 1 - Utilisation d'un tampon mémoire

```c
unsigned char *tmp;
/* Allocation memoire du tampon intermediaire : */
tmp = (unsigned char*) malloc(sizeof(unsigned char) *nbc*nbl);
```

Un tampon intermédiaire est nécessaire pour réaliser la convolution, car il est nécessaire de toujours avoir le pixel d'origine pour réaliser le calcul de convolution sur ses pixels voisins. Ainsi, si un pixel d'origine est modifié, sa valeur de départ ne pourra plus être utilisée pour le calcul, et ainsi la convolution résultant sera fausse. On utilise donc un tampon intermédiaire pour stocker les nouveaux pixels, à partir des valeurs de l'image de base.

## Question 2 - Séquences parallélisables

Sachant que l'on utilise un tampon intermédiaire pour le calcul de convolution, il est alors possible de parallélisé les boucles qui appliquent le filtre à l'image.

```c
int w, h; /* nombre de lignes et de colonnes de l'image */
/* Variables liees au traitement de l'image */
int filtre;	/* numero du filtre */
int nbiter;	/* nombre d'iterations */
/* La convolution a proprement parler */
for(i=0 ; i < nbiter ; i++){
    convolution(filtre, r.data, h, w);
}
```

## Question 3 - Compléxité et équilibrage

L'algorithme réalise un parcours de l'image I par double boucle. Sachant que la noyau k de convolution est de 3x3, la complexité théorique du calcul d'un pixel de I*k est donc constante et égale à O(1).

## Question 4 - Découpage 

Le temps de calcul de la convolution en un pixel étant constant, le déccoupage naturel du calcul entre les processeurs serait un découpage par bloc statique.

## Question 5 - Problèmes au bord

Entre chaque convolution, les valeurs au bord de l'image restent les mêmes qu'à l'origine. Ainsi, plus le nombre d'opérations de convolution est élevée, plus la convolution sera faussé car cette erreur s'étendra aux pixels voisins aux bords petit à petit, faussant les valeurs de ces derniers et s'étendant aux autres pixels voisins de ceux-ci.

## Question 6 - Résultats

Paramètres:
- Image "Albert-Einstein.ras" 1000*1200 px
- Filtre extracteur de contour
- 20 itérations

Nombre d'instances | Temps(s) | Accélération | Efficacité
--- | --- | --- | ---
1 | 6.01 | 1 | 1
2 | 3.14 | 1,914 | 0,957
4 | 1.65 | 3,642 | 0,911
7 | 1.06 | 5,669 | 0,809
14 | 0.7 | 8,586 | 0,613
25 | 0.6 | 10,017 | 0,401
28 | 0.58 | 10,362 | 0,37
50 | 1.3 | 4,623 | 0,092
100 | 3.43 | 1,752 | 0,017

![alt text](convol/images/TempsExecution.png "Temps d'éxecution")
![alt text](convol/images/Acceleration.png "Accélération")
![alt text](convol/images/TempsExecution.png "Efficacité")